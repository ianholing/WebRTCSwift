#WebRTCClient

Using WebRTC, WebRTCClient is a Swift application that allows a video conference system.

In order to do this, WebRTCClient use [WebRTCiOS library](http://stash.i2cat.net/projects/VC/repos/webrtcios/browse).

#How install

In WebRTCClient folder execute the following command in order to install dependencies:

```
pod install
```

#Push Notifications System

WebRTCClient has implemented a Push notification system. This system use [FCM](https://firebase.google.com/docs/cloud-messaging/?hl=es) from Google.

In [doc page of FCM](https://firebase.google.com/docs/cloud-messaging/ios/client?hl=es), developer can find how to implement one solution of this, but in this section can find a brief summary:


In appDelegate file:

```
func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    registerForPushNotifications(application)
    return true
}
func registerForPushNotifications(application: UIApplication){
    // Register for remote notifications
    if #available(iOS 8.0, *) {
        // [START register_for_notifications]
        let settings: UIUserNotificationSettings =
        UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        // [END register_for_notifications]
    } else {
        // Fallback
        let types: UIRemoteNotificationType = [.Alert, .Badge, .Sound]
        application.registerForRemoteNotificationTypes(types)
    }

    FIRApp.configure()

    // Add observer for InstanceID token refresh callback.
    NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.tokenRefreshNotification),
    name: kFIRInstanceIDTokenRefreshNotification, object: nil)

}



func tokenRefreshNotification(notification: NSNotification) {
    setTokenFirebaseCloudMessaging()
    connectToFcm()
}

func applicationDidEnterBackground(application: UIApplication) {
// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    FIRMessaging.messaging().disconnect()
    print("Disconnected from FCM.")
}

func applicationWillEnterForeground(application: UIApplication) {
// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

func applicationDidBecomeActive(application: UIApplication) {
// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    connectToFcm()
    application.applicationIconBadgeNumber = 0;
}



func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
    if notificationSettings.types != .None {
        application.registerForRemoteNotifications()
    }
}



func connectToFcm() {
    FIRMessaging.messaging().connectWithCompletion { (error) in
        if (error != nil) {
            print("Unable to connect with FCM. \(error)")
        } else {
            print("Connected to FCM.")
        }
    }
}

// [START receive_message]
func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
// If you are receiving a notification message while your app is in the background,
// this callback will not be fired till the user taps on the notification launching the application.


}
```

note:
If wants to change Firebase app in Firebase developer console, please change GoogleService-Info.plist

#Workflow

[Workflow](./images/VideoConferenceWorkflow.png)

@author: Carlos Morell Roldan