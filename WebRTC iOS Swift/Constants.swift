//
//  Constants.swift
//  WebRTC iOS Swift
//
//  Created by Carlos Morell Roldan on 23/7/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation

let IP = "192.168.1.221"
let PORT = "8081"
let SERVER_URL = "webrtcatdev.i2cat.net"
let SECURE_SERVER = "https://"
//let URL_BASE = "http://\(IP):\(PORT)"
let URL_BASE = "\(SECURE_SERVER)\(SERVER_URL):\(PORT)"
let URL_GET_LIST_USERS = "\(URL_BASE)/users"
let URL_GET_USER = "\(URL_BASE)/user/"
let URL_REGISTER = "\(URL_BASE)/user"
let URL_LOGIN = ""
let URL_NOTIFY_CALLEE = "\(URL_BASE)/notify"
let USER_KEY = "USERNAME"
let TOPIC_VC = "/topics/vc"

let INCOMING_CALL = "INCOMING_CALL"


enum STATE {
    case OK
    case REJECT
    case ERROR
}