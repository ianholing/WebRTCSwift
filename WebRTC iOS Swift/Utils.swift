//
//  Utils.swift
//  WebRTCClient
//
//  Created by Carlos Morell Roldan on 28/7/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation
import AVFoundation

class Utils {
    
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func isLogged()->Bool{
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if (defaults.stringForKey(USER_KEY) != nil){
            return true
        }
        return false

    }
    
    func getStateString(state:STATE)->String{
        
        if(state == STATE.ERROR){
            return "ERROR"
        }
        if(state == STATE.OK){
            return "OK"
        }
        if(state == STATE.REJECT){
            return "REJECT"
        }
        
        return "UNKOWN"
    }
    
    func timeString(time:NSTimeInterval) -> String {
        let minutes = Int(time) / 60
        //let seconds = Int(time) % 60
        let seconds = time - Double(minutes) * 60
        let secondsFraction = seconds - Double(Int(seconds))
        return String(format:"%02i:%02i.%01i",minutes,Int(seconds),Int(secondsFraction * 10.0))
    }
    
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer?  {
        //1
        let path = NSBundle.mainBundle().pathForResource(file as String, ofType: type as String)
        let url = NSURL.fileURLWithPath(path!)
        
        //2
        var audioPlayer:AVAudioPlayer?
        
        // 3
        do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }
}

