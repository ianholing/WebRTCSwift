//
//  LoginWebRTCViewController.swift
//  WebRTC iOS Swift
//
//  Created by Carlos Morell Roldan on 20/7/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//



import UIKit
import Firebase
import FirebaseInstanceID
import FirebaseMessaging


class LoginWebRTCViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var goToRegisterButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkRegister();
        initView()
        
        // Do any additional setup after loading the view.
    }
    
    func checkRegister(){
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let name = defaults.stringForKey(USER_KEY){
            goToMainView(name)
        }
    }
    
    func initView(){
        userName.delegate = self
        password.delegate = self
        loginButton.layer.cornerRadius = 10; // this value vary as per your desire
        loginButton.clipsToBounds = true;
        
        goToRegisterButton.layer.cornerRadius = 10; // this value vary as per your desire
        goToRegisterButton.clipsToBounds = true;
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        appDelegate.setTokenFirebaseCloudMessaging()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    
    @IBAction func goToRegisterViewAction(sender: UIButton) {
        
         self.performSegueWithIdentifier("registerView", sender: nil)
    }
    
    @IBAction func loginButton(sender: UIButton) {
       if let userNameValue: String = userName.text!{
            if !userNameValue.isEmpty{
                
                /*let _apiClient = ApiClient.sharedInstance.defaultApiClient
                _apiClient.request(.POST, URL_LOGIN, headers: nil, parameters:nil, encoding:.URL)
                    .responseJSON{ response in
                        print (response.result)
                        switch response.result{
                        case .Success(let json):
                            switch response.response!.statusCode{
                            case 200..<400: //OK
                                let my_resultJSON = JSON(json)
                                print("\(my_resultJSON)")
                                if(my_resultJSON.count != 0){
                                    self.processListUsers(my_resultJSON);
                                }
                            default:
                                print("error")
                            }
                        case .Failure(_):
                            print("error")
                        }
                }*/
                
                
                
                
                goToMainView(userNameValue)
            }else{
                showAlertWithMessage("username cannot be left blank")
            }
        }else{
            showAlertWithMessage("Enter the username")
        }
        
    }
    

    
    func showAlertWithMessage(message: String){
        let alertView: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
        alertView.addAction(alertAction)
        self.presentViewController(alertView, animated: true, completion: nil)
    }
    
    
    func goToMainView(userName: String){
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(userName, forKey: USER_KEY)
        User.sharedInstance.returnedUserName = userName
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.createMenuView()
    }
   
    
    
    
}

