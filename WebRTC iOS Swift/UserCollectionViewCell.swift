//
//  UserCollectionView.swift
//  WebRTC iOS Swift
//
//  Created by Carlos Morell Roldan on 21/7/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation


import UIKit

class UserCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
