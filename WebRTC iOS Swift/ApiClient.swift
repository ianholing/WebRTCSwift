//
//  ApiClient.swift
//  WebRTC iOS Swift
//
//  Created by Carlos Morell Roldan on 23/7/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation
import Alamofire

class ApiClient {
    static let sharedInstance = ApiClient()
    
    let defaultApiClient: Alamofire.Manager = {
       
        
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = Alamofire.Manager.defaultHTTPHeaders
        
        return Alamofire.Manager(
            configuration:configuration,
            serverTrustPolicyManager: nil
            )
    }()
}