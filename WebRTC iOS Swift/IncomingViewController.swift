//
//  IncomingViewController.swift
//  WebRTCClient
//
//  Created by Carlos Morell Roldan on 1/8/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

class IncomingViewController: UIViewController {
   
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var incomingUserCallName: UILabel!
    @IBOutlet weak var userIncomingCallImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        initView()
        initRoom()
        // Do any additional setup after loading the view.
    }
    
    func initRoom(){
        
        //let miliseconds:Int = Int(NSDate().timeIntervalSince1970)
        //let roomNameLabel = User.sharedInstance.returnedUserName.lowercaseString + user.getUserName().lowercaseString + String(miliseconds)
        
        
        let roomNameLabel = Room.sharedInstance.returnedRoomName
        
        
        incomingUserCallName.text = roomNameLabel
        
        Room.sharedInstance.returnedFromWho = "WYGTYaILITKale4s"
        if(Room.sharedInstance.returnedFromWho != ""){
            fetchIncomingUser()
        }
        
        //userNameLabel.text = user.getUserName()
    }
    
    func fetchIncomingUser(){
        
        let _apiClient = ApiClient.sharedInstance.defaultApiClient
        _apiClient.request(.GET, URL_GET_USER+Room.sharedInstance.returnedFromWho, headers: nil, parameters:nil, encoding:.URL)
            .responseJSON{ response in
                print (response.result)
                switch response.result{
                case .Success(let json):
                    switch response.response!.statusCode{
                    case 200..<400: //OK
                        let my_resultJSON = JSON(json)
                        print("\(my_resultJSON)")
                        if(my_resultJSON.count != 0){
                            
                        }
                    default:
                        print("error")
                    }
                case .Failure(_):
                    print("error")
                }
        }
        
    }
    func initView(){
        
        //createRoomButton.layer.cornerRadius = 10; // this value vary as per your desire
        //createRoomButton.clipsToBounds = true;
        
        /*createRoomButton.frame = CGRectMake(160, 100, 50, 50)
        createRoomButton.layer.cornerRadius = 0.5 * createRoomButton.bounds.size.width
        createRoomButton.setImage(UIImage(named:"call"), forState: .Normal)*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func acceptButtonAction(sender: UIButton) {
        if let roomNameValue: String = incomingUserCallName.text!{
            if !roomNameValue.isEmpty{
                self.performSegueWithIdentifier("acceptCall", sender: roomNameValue)
            }else{
                showAlertWithMessage("Room name cannot be left blank")
            }
        }else{
            showAlertWithMessage("Enter the room name")
        }
    }
    
    @IBAction func rejectButtonAction(sender: UIButton) {
        self.performSegueWithIdentifier("rejectCall", sender:nil)
    }
    
    func showAlertWithMessage(message: String){
        let alertView: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
        alertView.addAction(alertAction)
        self.presentViewController(alertView, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "acceptCall"){
            let webRTCVC: WebRTCViewController = segue.destinationViewController as! WebRTCViewController
            let data: String = sender as! String
            webRTCVC.roomName = data
        }
    }
    
}
