//
//  RoomNameViewController.swift
//  WebRTC iOS Swift
//
//  Created by Sushil Dahal on 2/8/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import UIKit
import SwiftyJSON

class RoomNameViewController: UIViewController,UITextFieldDelegate  {
    @IBOutlet weak var roomName: UITextField!
    
    var user: UserAgenda!
    @IBOutlet weak var createRoomButton: UIButton!
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        initView()
        initRoom()
                // Do any additional setup after loading the view.
    }
    
    

    func initRoom(){
        
        
        //let miliseconds:Int = Int(NSDate().timeIntervalSince1970)
        //let roomNameLabel = User.sharedInstance.returnedUserName.lowercaseString + user.getUserName().lowercaseString + String(miliseconds)
        var roomNameLabel = ""
        
            let miliseconds:Int = Int(NSDate().timeIntervalSince1970)

            roomNameLabel = User.sharedInstance.returnedUserName.lowercaseString + user.getUserName().lowercaseString + String(miliseconds)
        
            
            Room.sharedInstance.returnedRoomName = roomNameLabel
        
        
        
        
        roomName.text = roomNameLabel
        
        userNameLabel.text = user.getUserName()
    }
    
    func initView(){
        
        //createRoomButton.layer.cornerRadius = 10; // this value vary as per your desire
        //createRoomButton.clipsToBounds = true;

        createRoomButton.frame = CGRectMake(160, 100, 50, 50)
        createRoomButton.layer.cornerRadius = 0.5 * createRoomButton.bounds.size.width
        createRoomButton.setImage(UIImage(named:"call"), forState: .Normal)
        
        /*rejectButton.frame = CGRectMake(160, 100, 50, 50)
        rejectButton.layer.cornerRadius = 0.5 * createRoomButton.bounds.size.width
        rejectButton.setImage(UIImage(named:"hangup"), forState: .Normal)*/
        
        
        
        /*
        if(!isCaller){
            rejectButton.hidden = false
        }*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func connectButton(sender: UIButton) {
        //if(isCaller){
            makeCall()
        /*}else{
            acceptCall()
        }*/
    }
    
    func acceptCall(){
        if let roomNameValue: String = roomName.text!{
            if !roomNameValue.isEmpty{
                self.performSegueWithIdentifier("connectToRoom", sender: roomNameValue)
            }
        }
    }
    
    func makeCall(){
        
        if let roomNameValue: String = roomName.text!{
            if !roomNameValue.isEmpty{
                
                let _apiClient = ApiClient.sharedInstance.defaultApiClient
                
                
                let parameters = [
                    "notifToken": user.getNotifToken(),
                    "roomName":roomNameValue,
                    "idCaller":user.getId()
                ]
                
                _apiClient.request(.POST, URL_NOTIFY_CALLEE, headers: nil, parameters:parameters, encoding:.JSON)
                    .responseJSON{ response in
                        print (response.result)
                        switch response.result{
                        case .Success(let json):
                            switch response.response!.statusCode{
                            case 200..<400: //OK
                                let my_resultJSON = JSON(json)
                                print("\(my_resultJSON)")
                                self.performSegueWithIdentifier("connectToRoom", sender: roomNameValue)
                            default:
                                print("error")
                            }
                        case .Failure(_):
                            print("error")
                        }
                }
                
                
            }else{
                showAlertWithMessage("Room name cannot be left blank")
            }
        }else{
            showAlertWithMessage("Enter the room name")
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    func showAlertWithMessage(message: String){
        let alertView: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
        alertView.addAction(alertAction)
        self.presentViewController(alertView, animated: true, completion: nil)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "connectToRoom"){
            let webRTCVC: WebRTCViewController = segue.destinationViewController as! WebRTCViewController
            let data: String = sender as! String
            webRTCVC.roomName = data
            webRTCVC.isCaller = true
        }
    }

}
