//
//  RegisterWebRTCViewController.swift
//  WebRTCClient
//
//  Created by Carlos Morell Roldan on 27/7/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class RegisterWebRTCViewController: UIViewController,UITextFieldDelegate {
   
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
        // Do any additional setup after loading the view.
    }
    
    
    func initView(){
        repeatPasswordTextField.delegate = self
        passwordTextField.delegate = self
        emailTextField.delegate = self
        nameTextField.delegate = self
        usernameTextField.delegate = self
        registerButton.layer.cornerRadius = 10; // this value vary as per your desire
        registerButton.clipsToBounds = true;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        textField.resignFirstResponder()
        return true;
    }
    
    
    @IBAction func registerButton(sender: UIButton) {
        
        if(checkFields()){
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let token:String = appDelegate.getTokenFirebaseCloudMessaging()
            
            
            let username:String = usernameTextField.text!
            
            let parameters = [
                "notif_token": token,
                "username":username
            ]
            
            let _apiClient = ApiClient.sharedInstance.defaultApiClient
                _apiClient.request(.POST, URL_REGISTER, headers: nil, parameters:parameters, encoding:.JSON)
                     .responseJSON{ response in
                     print (response.result)
                     switch response.result{
                        case .Success(let json):
                            switch response.response!.statusCode{
                             case 200..<400: //OK
                                 let my_resultJSON = JSON(json)
                                 print("\(my_resultJSON)")
                                 if(my_resultJSON.count != 0){
                                    
                                    if(my_resultJSON["error"] != nil){
                                        self.showAlertWithMessage(my_resultJSON["error"].stringValue)
                                    }else{
                                        //correct, go to login view
                                        self.performSegueWithIdentifier("goToLogin", sender: nil)
                                    }
                                    
                                 }
                             default:
                                 print("error")
                             }
                        case .Failure(_):
                             print("error")
                 }
             }
            
        }
        
    }
    
    func checkFields()->Bool{
        
        let isCorrect = true
        
        
        
       /* if let nameField: String = nameTextField.text!{
            if nameField.isEmpty{
                
                showAlertWithMessage("Name cannot be left blank")
                return false
            }
        }else{
            
            showAlertWithMessage("Enter the name")
            return false
        }
        
        if let userName: String = usernameTextField.text!{
            if userName.isEmpty{
                
                showAlertWithMessage("Username cannot be left blank")
                return false
            }
        }else{
            
            showAlertWithMessage("Enter the username")
            return false
        }
        
        if let emailField: String = emailTextField.text!{
            if emailField.isEmpty{
                
                showAlertWithMessage("Email cannot be left blank")
                return false
            }
        }else{
            
            showAlertWithMessage("Enter the email")
            return false
        }
        
        let emailField: String = emailTextField.text!
        
        if (!Utils().isValidEmail(emailField)){
            showAlertWithMessage("Incorrect email address")
            return false

        }
        
        if let passwordField: String = passwordTextField.text!{
            if passwordField.isEmpty{
                
                showAlertWithMessage("Password cannot be left blank")
                return false
            }
        }else{
            
            showAlertWithMessage("Enter password")
            return false
        }
        
        if let repeatPasswordField: String = repeatPasswordTextField.text!{
            if repeatPasswordField.isEmpty{
                
                showAlertWithMessage("Repeat Password cannot be left blank")
                return false
            }
        }else{
            
            showAlertWithMessage("Enter repeat password")
            return false
        }
        
        
        
        let repeatPasswordField: String = repeatPasswordTextField.text!
        let password: String = passwordTextField.text!
        
        if(password.length < 6){
            showAlertWithMessage("Password must have at least 6 characters")
            return false
            
        }
        
        if (repeatPasswordField != password){
            showAlertWithMessage("Repeat Password must be the same that password")
            return false
        }*/
        
        
        return isCorrect;
        
        
        
    }
    
    func showAlertWithMessage(message: String){
        let alertView: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
        alertView.addAction(alertAction)
        self.presentViewController(alertView, animated: true, completion: nil)
    }
    
    
   
    
    
    
    
}