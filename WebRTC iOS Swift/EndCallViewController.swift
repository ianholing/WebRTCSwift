//
//  EndCallViewController.swift
//  WebRTCClient
//
//  Created by Carlos Morell Roldan on 11/8/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation


class EndCallViewController: UIViewController {

    @IBOutlet weak var stateEndCallText: UITextField!
    @IBOutlet weak var durationCallText: UITextField!
    @IBOutlet weak var roomNameText: UITextField!
    var user: UserAgenda!
    var state:STATE!
    var roomName:String!
    var time:NSTimeInterval!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBarHidden = true
               // Do any additional setup after loading the view.
        
        state = STATE.ERROR
        time = 0.0
        roomName = "test"
        stateEndCallText.text = Utils().getStateString(state)
        roomNameText.text = roomName
        durationCallText.text = Utils().timeString(time)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
    }

}