//
//  WebRTCViewController.swift
//  WebRTC iOS Swift
//
//  Created by Sushil Dahal on 2/2/16.
//  Copyright © 2016 Sushil Dahal.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//    
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import UIKit
import AVFoundation

// The view controller that is displayed when WebRTC iOS Swift is loaded.
class WebRTCViewController: UIViewController, ARDAppClientDelegate, RTCEAGLVideoViewDelegate {
    
    @IBOutlet weak var remoteView: RTCEAGLVideoView!
    @IBOutlet weak var localView: RTCEAGLVideoView!
    
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    @IBOutlet weak var RoomNameLabel: UILabel!
    
    @IBOutlet weak var incomingView: UIView!
    @IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var videoConference: UIView!
    
    var roomName: String!
    var client: ARDAppClient?
    var localVideoTrack: RTCVideoTrack?
    var remoteVideoTrack: RTCVideoTrack?
    
    var callState:STATE?
    
    var callMusic : AVAudioPlayer?

    
    var isMute:Bool = false
    var frontInUse:Bool = true
    var isCaller:Bool = false
    
    var timer:NSTimer!
    let timeInterval:NSTimeInterval = 0.05
    var timeCount:NSTimeInterval = 0.0
    
    

    @IBOutlet weak var muteUnMuteButton: UIButton!
    @IBOutlet weak var hangUpButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hangUpButton.layer.cornerRadius = 5; // this value vary as per your desire
        hangUpButton.clipsToBounds = true;
        muteUnMuteButton.layer.cornerRadius = 5; // this value vary as per your desire
        muteUnMuteButton.clipsToBounds = true;
        
        
        videoConference.hidden = true
        waitingView.hidden = true
        incomingView.hidden = true
        
        timer = NSTimer()
        timeCount = 0.0
        
        if let _callMusic = Utils().setupAudioPlayerWithFile("1985_ring", type:"mp3") {
            callMusic = _callMusic
        }

        
        if(isCaller){
            callMusic?.play()
            waitingView.hidden = false
        }else{
             incomingView.hidden = false
        }
        initView()
       
        // Do any additional setup after loading the view, typically from a nib.
        initialize()
        connectToChatRoom()
    }
    
    func initView(){
        
        acceptButton.frame = CGRectMake(160, 100, 50, 50)
        acceptButton.layer.cornerRadius = 0.5 * acceptButton.bounds.size.width
        acceptButton.setImage(UIImage(named:"call"), forState: .Normal)
        
        rejectButton.frame = CGRectMake(160, 100, 50, 50)
        rejectButton.layer.cornerRadius = 0.5 * rejectButton.bounds.size.width
        rejectButton.setImage(UIImage(named:"hangup"), forState: .Normal)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        disconnect()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func endButton(sender: UIButton) {
        disconnect()
        self.performSegueWithIdentifier("endCall", sender: nil)
    }
    
    func endCall(){
        disconnect()
    }
    
    
    @IBAction func sendOfferCall(sender: UIButton){
        client?.startCalling()
        callMusic?.stop()

    }
    
    @IBAction func rotateCamera(sender: UIButton) {
        if(self.frontInUse){
            client?.swapCameraToBack()
           
            
        }else{
            client?.swapCameraToFront()
        }
        
        self.frontInUse = !self.frontInUse
        
        
       
    }
    
    @IBAction func sendRejectCall(sender: UIButton) {
        disconnect()
        callMusic?.stop()
        self.performSegueWithIdentifier("endCall", sender: nil)
    }
    
    @IBAction func muteUnMutteButton(sender: UIButton) {
        if(self.isMute){
            client?.unmuteAudioIn()
            muteUnMuteButton.setBackgroundImage(UIImage(named:"unmute"), forState: .Normal)
            
        }else{
            client?.muteAudioIn()
            muteUnMuteButton.setBackgroundImage(UIImage(named:"mute"), forState: .Normal)
        }
        
        self.isMute = !self.isMute
    }
    
//    MARK: RTCEAGLVideoViewDelegate
    func appClient(client: ARDAppClient!, didChangeSignalingState state: ARDAppClientState) {
        switch state{
        case ARDAppClientState.Connected:
            print("Client Connected")
            break
        case ARDAppClientState.Connecting:
            print("Client Connecting")
            break
        case ARDAppClientState.Disconnected:
            print("Client Disconnected")
            remoteDisconnected()
        }
    }
    
    func appClient(client: ARDAppClient!, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack!) {
        self.localVideoTrack = localVideoTrack
        self.localVideoTrack?.addRenderer(localView)
    }
    
    func appClient(client: ARDAppClient!, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack!) {
        self.remoteVideoTrack = remoteVideoTrack
        self.remoteVideoTrack?.addRenderer(remoteView)
    }
    
    func appClient(client: ARDAppClient!, didError error: NSError!) {
//        Handle the error
        showAlertWithMessage(error.localizedDescription)
        disconnect()
        stopTimer()
    }
    
    
    func appClient(client: ARDAppClient!, callEnd missatge: String!) {
        stopTimer()
        //callState = STATE.OK
        self.performSegueWithIdentifier("endCall", sender: nil)
    }
    
    func appClient(client: ARDAppClient!, callStart missatge: String!){
        videoConference.hidden = false
        waitingView.hidden = true
        incomingView.hidden = true
        stopTimer()
        startTimer()
    }
    
//    MARK: RTCEAGLVideoViewDelegate
    
    func videoView(videoView: RTCEAGLVideoView!, didChangeVideoSize size: CGSize) {
//        Resize localView or remoteView based on the size returned
    }
    
//    MARK: Private
    
    func initialize(){
        disconnect()
//        Initializes the ARDAppClient with the delegate assignment
        client = ARDAppClient.init(delegate: self)
        
//        RTCEAGLVideoViewDelegate provides notifications on video frame dimensions
        remoteView.delegate = self
        localView.delegate = self
    }
    
    func connectToChatRoom(){
        print("Connection to room"+roomName)
        client?.serverHostUrl = "https://webrtcatdev.i2cat.net:8443"
        
        client?.setSTUNServer("stun:webrcatdev.i2cat.net:3478")
        
        client?.addRTCICEServer("turn:webrcatdev.i2cat.net:3478?transport=udp", username:"i2cat", password:"i2catdev")
        
         client?.addRTCICEServer("turn:webrcatdev.i2cat.net:3478?transport=tcp", username:"i2cat", password:"i2catdev")
        
        client?.connectToRoomWithId(roomName, options: nil)
    }
    
    func remoteDisconnected(){
        if(remoteVideoTrack != nil){
            remoteVideoTrack?.removeRenderer(remoteView)
        }
        remoteVideoTrack = nil
    }
    
    func disconnect(){
        if(client != nil){
            if(localVideoTrack != nil){
                localVideoTrack?.removeRenderer(localView)
            }
            if(remoteVideoTrack != nil){
                remoteVideoTrack?.removeRenderer(remoteView)
            }
            localVideoTrack = nil
            remoteVideoTrack = nil
            client?.disconnect()
        }
    }
    
    func showAlertWithMessage(message: String){
        let alertView: UIAlertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
        alertView.addAction(alertAction)
        self.presentViewController(alertView, animated: true, completion: nil)
    }
    
    func startTimer(){
        timeCount = 0.0
        timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval,
                                                       target: self,
                                                       selector: "timerCounter:",
                                                       userInfo: nil,
                                                       repeats: true) //repeating timer in the second iteration
    }
    
    func stopTimer(){
        if(timer != nil){
            timer.invalidate()
        }
    }
    
    func timerCounter(timer:NSTimer){
        
        timeCount = timeCount + timeInterval
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "endCall"){
            let endCallController: EndCallViewController = segue.destinationViewController as! EndCallViewController
            
            endCallController.roomName = roomName
            
           
            endCallController.time = timeCount
            
            
            endCallController.state = callState
            
            
           
        }
        
    }
    
}

