//
//  Room.swift
//  WebRTCClient
//
//  Created by Carlos Morell Roldan on 1/8/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation


class Room {
    
    var roomName : String = ""
    var fromWho: String = ""
    
    class var sharedInstance : Room {
        struct Static {
            static let instance : Room = Room()
        }
        return Static.instance
    }
    
    var returnedRoomName : String {
        get{
            return self.roomName
        }
        
        set {
            self.roomName = newValue
        }
    }
    
    var returnedFromWho : String {
        get{
            return self.fromWho
        }
        
        set {
            self.fromWho = newValue
        }
    }
}
