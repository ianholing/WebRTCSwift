//
//  UserContactListViewCell.swift
//  WebRTCClient
//
//  Created by Carlos Morell Roldan on 29/7/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation


class UserContactListViewCell: UITableViewCell {

    @IBOutlet weak var userContactImage: UIImageView!
    @IBOutlet weak var userContactName: UILabel!
    @IBOutlet weak var userContactStatus: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}