//
//  AboutViewController.swift
//  WebRTC iOS Swift
//
//  Created by Carlos Morell Roldan on 21/7/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation


class AboutViewController: UIViewController {
    
    override func viewDidLoad() {
         self.title = "About WebRTCat"
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
}