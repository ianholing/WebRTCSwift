//
//  AppDelegate.swift
//  WebRTC iOS Swift
//
//  Created by Sushil Dahal on 2/2/16.
//  Copyright © 2016 Sushil Dahal.
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import UIKit
import SlideMenuControllerSwift
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        // Enable SSL globally for WebRTC in our app
        RTCPeerConnectionFactory.initializeSSL()
        registerForPushNotifications(application)
        return true
        
        
    }
   
    func registerForPushNotifications(application: UIApplication){
        // Register for remote notifications
        if #available(iOS 8.0, *) {
            // [START register_for_notifications]
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
            // [END register_for_notifications]
        } else {
            // Fallback
            let types: UIRemoteNotificationType = [.Alert, .Badge, .Sound]
            application.registerForRemoteNotificationTypes(types)
        }
        
        FIRApp.configure()
        
        // Add observer for InstanceID token refresh callback.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                                         name: kFIRInstanceIDTokenRefreshNotification, object: nil)
        
       

    
        
    }
    
    func setTokenFirebaseCloudMessaging(){
        if let token = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(token)")
            User.sharedInstance.returnedNotifToken = token
            //FIRMessaging.messaging().subscribeToTopic("/topics/vc")
        }
    }
    
    func getTokenFirebaseCloudMessaging()->String{
        
        if (User.sharedInstance.returnedNotifToken != ""){
            return User.sharedInstance.returnedNotifToken
        }else{
            if let token = FIRInstanceID.instanceID().token() {
                return token
            }
            return ""
        }
        
        
    }
    func tokenRefreshNotification(notification: NSNotification) {
        
        setTokenFirebaseCloudMessaging()
                    
        connectToFcm()
                
    }
    func createMenuView(){
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("ListUsersViewController") as! ListUsersViewController
        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("SideMenuViewController") as! SideMenuViewController
       
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = UIColor(hex: "F9AD1D")
        
        
        leftViewController.listUsersViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
    }
    
    func goCall(roomId:String, callerId:String){
        
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("ListUsersViewController") as! ListUsersViewController
        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("SideMenuViewController") as! SideMenuViewController
        
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = UIColor(hex: "F9AD1D")
        
        
        leftViewController.listUsersViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        self.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
        
        
        Room.sharedInstance.returnedRoomName = roomId as String
        Room.sharedInstance.returnedFromWho = callerId as String
        
        mainViewController.isCaller = false
        
        mainViewController.moveToCall()
        
    }
   

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        connectToFcm()
        application.applicationIconBadgeNumber = 0;

    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        RTCPeerConnectionFactory.deinitializeSSL()
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .None {
            application.registerForRemoteNotifications()
        }
    }
    
    
    
    func connectToFcm() {
        FIRMessaging.messaging().connectWithCompletion { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    
    /**
     { type: "INCOMING_CALL", startMain: true, roomName: roomName }
     **/
    
    // [START receive_message]
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
                     fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        
        //UIApplication.sharedApplication().applicationIconBadgeNumber = 0

        if(Utils().isLogged()){
            if let type = userInfo["type"] as? NSString{
                 print("el type es:"+(type as String))
                
                switch type {
                case INCOMING_CALL:
                    let roomId:NSString = (userInfo["roomName"] as? NSString!)!
                    var callerId:NSString = ""
                    if((userInfo["callerId"]) != nil){
                        callerId = (userInfo["callerId"] as? NSString!)!
                    }
                    
                    print("RoomName:"+(roomId as String))
                    print("From user:"+(callerId as String))
                    
                   
                    
                    goCall(roomId as String, callerId: callerId as String);

                    
                    break
                    
                default:
                    
                    break
                }
            }
        }
        print("%@", userInfo)
        completionHandler(UIBackgroundFetchResult.NewData)
        
    }
    
    
    
    
    

}

