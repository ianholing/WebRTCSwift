//
//  ListUsersViewController.swift
//  WebRTC iOS Swift
//
//  Created by Carlos Morell Roldan on 21/7/16.
//  Copyright © 2016 Sushil Dahal. All rights reserved.
//

import Foundation
import SlideMenuControllerSwift
import SwiftyJSON
import AVFoundation

class ListUsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var userContactList: UITableView!
    //@IBOutlet weak var listUsers: UICollectionView!
    @IBOutlet weak var call: UIButton!
    
    var isCaller = false
    
    @IBOutlet weak var testTimer: UILabel!
    var arrayAgendaUsers:NSMutableArray = NSMutableArray()
    var callMusic : AVAudioPlayer?
    
    var timer:NSTimer!
    let timeInterval:NSTimeInterval = 0.05
    var timeCount:NSTimeInterval = 0.0
    
    override func viewDidLoad() {
        self.title = "WebRTCat list Users"
        super.viewDidLoad()
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 90, height: 90)
        
        
        getAgendaUsers()
        
        
        
               
    }
    
    @IBAction func startTimer(sender: UIButton) {
        timeCount = 0.0
        timer = NSTimer.scheduledTimerWithTimeInterval(timeInterval,
                                                       target: self,
                                                       selector: "timerCounter:",
                                                       userInfo: nil,
                                                       repeats: true) //repeating timer in the second iteration
    }
    
    @IBAction func stopTimer(sender: UIButton) {
        timer.invalidate()
        testTimer.text = Utils().timeString(timeCount)
       
        
    }
    
    func timerCounter(timer:NSTimer){
        
        timeCount = timeCount + timeInterval
    }
    
    
    
    func moveToCall(){
        
        self.performSegueWithIdentifier("callUser", sender: Room.sharedInstance.returnedRoomName)
        
    }
    
    
    func getAgendaUsers(){
        
        let _apiClient = ApiClient.sharedInstance.defaultApiClient
        _apiClient.request(.GET, URL_GET_LIST_USERS, headers: nil, parameters:nil, encoding:.URL)
            .responseJSON{ response in
                print (response.result)
                switch response.result{
                case .Success(let json):
                    switch response.response!.statusCode{
                    case 200..<400: //OK
                        let my_resultJSON = JSON(json)
                        print("\(my_resultJSON)")
                        if(my_resultJSON.count != 0){
                            self.processListUsers(my_resultJSON);
                        }
                    default:
                        print("error")
                    }
                case .Failure(_):
                    print("error")
                }
        }
        
       
        
        
        
        
    }
    
    func processListUsers(json:JSON){
        
        let userToken:String = User.sharedInstance.returnedNotifToken
        
        for item in json.arrayValue {
            let _user:UserAgenda = UserAgenda()
            print(item["username"].stringValue)
            let currentToken:String = item["notif_token"].stringValue
            if userToken != currentToken {
                _user.setUserName(item["username"].stringValue)
                _user.setNotifToken(currentToken)
                _user.setId(item["_id"].stringValue)
                arrayAgendaUsers.addObject(_user)
            }
            
        }

        
        
        self.userContactList.reloadData();
        
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.setNavigationBarItem()
    }
    
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell:UserContactListViewCell = self.userContactList.dequeueReusableCellWithIdentifier("usercell") as! UserContactListViewCell
        
         let user:UserAgenda = arrayAgendaUsers.objectAtIndex(indexPath.row) as! UserAgenda
        
        cell.userContactName.text = user.getUserName()
        cell.userContactImage.image = UIImage(named: "webrtcIcon")
       
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayAgendaUsers.count;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
       
        
        let user:UserAgenda = arrayAgendaUsers.objectAtIndex(indexPath.row) as! UserAgenda
        
         callUser(user)
        }
    
    func callUser(user:UserAgenda){
        
       
        
        
        self.performSegueWithIdentifier("createRoom", sender: user)
        
        
        
        
        
        
    }
    
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "createRoom"){
            let roomController: RoomNameViewController = segue.destinationViewController as! RoomNameViewController
            let data: UserAgenda = sender as! UserAgenda
            roomController.user = data
        }
        if(segue.identifier == "callUser"){
            let webRTCViewController: WebRTCViewController = segue.destinationViewController as! WebRTCViewController
            webRTCViewController.isCaller = false
            webRTCViewController.roomName = Room.sharedInstance.returnedRoomName
        }
    }
    
}

extension ListUsersViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}